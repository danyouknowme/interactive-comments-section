export interface props {
    image: string,
    username: string,
    createdAt: string,
    content: string,
    score: number
  }