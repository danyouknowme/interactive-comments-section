import React, { FC } from "react";
import { Row, Tag, Avatar, Card, Typography, Space } from "antd";
import { props } from '../types/props';
import "./card-comment.scss"; 

const CardComment: FC<props> = (props) => {
  return (
    <>
      <Card className="comment-container">
        <div className="comment-wrapper">
          <Row className="comment-column">
            <Space size={18}>
              <Avatar
                size={40}
                className="picture-profile"
                src={props.image}
              />
              <Typography.Title level={5}>{props.username}</Typography.Title>
              <Typography.Title level={5} className="created-time">
                {props.createdAt}
              </Typography.Title>
            </Space>
          </Row>
          <Row className="comment-column">
            <Typography.Paragraph className="comment-description">
              {props.content}
            </Typography.Paragraph>
          </Row>
        </div>
        <Row className="comment-column">
            <Tag className="tag">
              <Row>
                <img src="/images/icon-plus.svg" alt="plus" />
                <Typography.Title level={5}>{props.score}</Typography.Title>
                <img src="/images/icon-minus.svg" alt="minus" />
              </Row>
            </Tag>
        </Row>
        <Row className="reply">
          <img src="/images/icon-reply.svg" alt="reply" />
          <Typography.Title level={5}>Reply</Typography.Title>
        </Row>
      </Card>
    </>
  );
};

export default CardComment;
