import React from "react";
import CardComment from "./components/CardComment";
import { data } from './data';

const App = () => {
	return (
		<div className="app" style={{backgroundColor: 'whitesmoke'}}>
			{data.comments.map((props) => (
				<CardComment image={props.user.image.png} username={props.user.username} createdAt={props.createdAt} content={props.content} score={props.score} key={props.id}/>
			))}
		</div>
	);
};

export default App;
